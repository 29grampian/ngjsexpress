import path from 'path'
import express from 'express'

const app = express()
const DIST_DIR = __dirname
const HTML_FILE = path.join(DIST_DIR, 'index.html')
const API_DIR = 
app.use(express.static(DIST_DIR))

app.use('/api', (req, res, next)=>{
    require(__dirname+'/../api/index')(req, res, next)
})

app.get('/cat', (req, res) => {
    res.send('i am a cat')
})

app.get('/dog', (req, res) => {
    res.send('i am a dog')
})

app.get('*', (req, res) => {
    res.sendFile(HTML_FILE)
})

const PORT = process.env.PORT || 8080

app.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`)
    console.log('__dirname: ' + __dirname);
    console.log('Press Ctrl+C to quit.')
})